import * as React from 'react';
import Head from 'next/head';
import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { CacheProvider } from '@emotion/react';
import theme from '../src/util/theme';
import createEmotionCache from '../src/createEmotionCache';
import { useState} from "react";
import { SnackbarProvider } from 'notistack'

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();

export default function MyApp(props) {
    // console.log('Router --> ',Router);
    const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;

    const [loading, setLoading] = useState(false);

   

    return (
        <CacheProvider value={emotionCache}>
            <Head>
                <title>{`${Component.title ? Component.title + ' - ' : ''}Test Project`}</title>
                <meta name="viewport" content="initial-scale=1, width=device-width" />
            </Head>
            <SnackbarProvider anchorOrigin={{
                horizontal: 'center',
                vertical: 'bottom'
            }}>
                <ThemeProvider theme={theme}>
                    {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
                    <CssBaseline />
                    
                    {
                        loading ? <></> : <Component {...pageProps} />
                    }
                </ThemeProvider>
            </SnackbarProvider>
        </CacheProvider>
    );
}


