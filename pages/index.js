import { CssBaseline, Stack } from '@mui/material'
import React from 'react'
import AppBar from '../src/components/AppBar'
import Desktop from '../src/components/desktop'
import Category from '../src/components/Category'
import Product from '../src/components/Product'
import Package from '../src/components/package'
import Benefits from '../src/components/Benefits'
import Testimonials from '../src/components/Testimonials'
import Newslatter from '../src/components/Newslatter'
import Footer from '../src/components/Footer'
import Creation from '../src/components/creation'
import Abcd from '../src/components/abcd'
// import { Carousel } from 'react-responsive-carousel'
import Slider from 'react-slick'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

function index() {
  const list = [
    {
      image: '/images/testimonials/girl.svg',
      about: '“My experience with Mark is a complete  sucess, from customer service, wide range of  products, clean store, purchasing experience, the newsletter.Thank you.”',
      name: ' Leona Paul',
      dept: 'CEO of Floatcom'
    },
    {
      image: '/images/testimonials/girl.svg',
      about: '“My experience with Mark is a complete  sucess, from customer service, wide range of  products, clean store, purchasing experience, the newsletter.Thank you.”',
      name: ' Leona Paul',
      dept: 'CEO of Floatcom'
    },
    {
      image: '/images/testimonials/girl.svg',
      about: '“My experience with Mark is a complete  sucess, from customer service, wide range of  products, clean store, purchasing experience, the newsletter.Thank you.”',
      name: ' Leona Paul',
      dept: 'CEO of Floatcom'
    },
  ]


  const settings = {
    // dots: true,
    // infinite: true,
    // speed: 0,
    // slidesToShow: 1,
    slidesToScroll: 1
  };


  return (

    <>
      <CssBaseline />
      <Stack width={'100%'} >
        <AppBar />
        <Desktop />
        <Category />
        <Product />
        <Package />
        <Creation />
        <Benefits />
        <Stack width={'98%'}>
          <Slider {...settings}>
            {
              list.map((each) => {
                return (
                  <Testimonials each={each} />
                )
              })
            }
          </Slider>
        </Stack>
        <Newslatter />
        <Footer />
      </Stack >
    </>
  )
}

export default index


