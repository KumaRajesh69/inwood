import { Avatar, Box, Button, Card, CardActions, Grid, Stack, Typography } from '@mui/material'
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import React from 'react'

const chair = [
    {
        img: './images/product/chair1.svg'
    },
    {
        img: './images/product/chair2.svg'
    },
    {
        img: './images/product/chair3.svg'
    },
    {
        img: './images/product/chair4.svg'
    },
    {
        img: './images/product/chair2.svg'
    },
    {
        img: './images/product/chair3.svg'
    },
    {
        img: './images/product/chair4.svg'
    },
    {
        img: './images/product/chair4.svg'
    }, {
        img: './images/product/chair2.svg'
    },
    {
        img: './images/product/chair3.svg'
    },
    {
        img: './images/product/chair4.svg'
    },
    {
        img: './images/product/chair4.svg'
    }, {
        img: './images/product/chair2.svg'
    },
    {
        img: './images/product/chair3.svg'
    },
    {
        img: './images/product/chair4.svg'
    },


]
function Product() {
    return (
        <>
            <Stack width='100%' sx={{ textAlign: 'center', justifyContent: 'center' }}>
                <Typography sx={{
                    fontSize: 40, fontWeight: 'bold',
                    fontFamily: 'Playfair Display',
                    textAlign: 'center', justifyContent: 'center'
                }}>
                    Popular Products
                </Typography>
                <Stack direction='row' sx={{ mt: 10, alignItems: 'center',overflow:'hidden' }}>
                    <Box sx={{ position: 'absolute' }}>
                        <Avatar variant='square'
                            src='/images/product/Leftchair.svg'
                            sx={{ height: '100vh', width: '200px', }} />
                    </Box>
                    <Stack alignItems='center' width='100%' sx={{ zIndex: 100, position: 'relative', left: {xs:220,sm:10,md:100,lg:0} }}>
                        <Box sx={{ width: '1200px', overflowX: 'auto', scrollBehavior: 'smooth', ml: 2 }}>
                            <Stack direction={'row'}
                                sx={{ width: { xs: '500px', md: '700px', lg: "100%" } }} spacing={2}>
                                {chair.map((each) => {
                                    return (
                                        <Stack direction={'row'} >
                                            <Card sx={{
                                                width: "250px",
                                                objectFit: 'cover',
                                                backgroundPosition: 'center',
                                            }}>
                                                <Avatar variant='square' src={each.img} sx={{ height: '400px', width: '100%' }} />
                                            </Card>
                                        </Stack>
                                    );
                                })}
                            </Stack>
                        </Box>
                    </Stack>

                </Stack >

                <Box width='100%' sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <Button variant="contained"
                        endIcon={<ArrowForwardIcon style={{ height: '17px' }} />}
                        sx={{ width: '180px', height: '50px', mt: 10, bgcolor: '#70908B', }}>
                        <Typography sx={{ fontSize: 12, fontWeight: '500', color: '#ffffff', textTransform: ' capitalize' }}>
                            Explore all items
                        </Typography>
                    </Button>
                </Box>
            </Stack >
        </>
    )
}

export default Product






{/* <ScrollMenu LeftArrow={LeftArrow} RightArrow={RightArrow}>
                                    {items.map(({ id }) => (
                                        <Card
                                            itemId={id} // NOTE: itemId is required for track items
                                            title={id}
                                            key={id}
                                            onClick={handleClick(id)}
                                            selected={isItemSelected(id)}
                                        />
                                    ))}
                                </ScrollMenu> */}