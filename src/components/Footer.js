import { Avatar, Grid, Stack, Typography } from '@mui/material'
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import SportsBaseballIcon from '@mui/icons-material/SportsBaseball';
import TwitterIcon from '@mui/icons-material/Twitter';
import React from 'react'

const list = [
    {
        titel: './images/appbar/Logo 1.svg',
        address: '+123 654 987877  The Bronx, NY14568, USA',
        heading: true,
    },
    // {
    //     name: 'Address',
    //     obj1: '+123 654 987',
    //     obj2: ' 877  The Bronx, NY',
    //     obj3: '14568, USA'
    // },
    {
        name: 'My Account',
        obj1: 'Sign in',
        obj2: 'Register',
        obj3: 'Order status'
    },
    {
        name: 'Help',
        obj1: 'Shipping',
        obj2: 'Returns',
        obj3: 'Sizing'
    },
    {
        name: 'Shop',
        obj1: 'All Products',
        obj2: ' Bedroom',
        obj3: 'Dinning Room'
    },
    {
        name: 'Legal Stuff',
        obj1: 'Shipping & Delivery',
        obj2: 'Terms & Conditions',
        obj3: 'Privacy & Policy'
    },

]


function Footer() {
    return (
        <>
            <Grid container sx={{ px: { xs: 2, sm: 5, md: 6, lg: 13 }, pt: 10, pb: 3, bgcolor: '#F3F6F5' }}>
                {list.map((each) => {
                    return (
                        each.heading ? <Grid item xs={12} sm={12} md={4} lg={4}>
                            <Avatar variant='square' src={each.titel} sx={{
                                height: {xs:'38px',sm:'30px' ,md: '33px', lg: '26px' },
                                width: {  xs: '150px', sm: '120px',md: '150px', lg: '100px' }
                            }} />
                            <Stack direction='row' spacing={3} sx={{ my: 2, }}>
                                <FacebookIcon sx={{ color: '#07484A', fontSize: {xs:'40px',sm:'35px', md: '30px', lg: '30px' } }} />
                                <InstagramIcon sx={{ color: '#07484A', fontSize:{xs:'40px',sm:'35px', md: '30px', lg: '30px' }}} />
                                <LinkedInIcon sx={{ color: '#07484A', fontSize: {xs:'40px',sm:'35px', md: '30px', lg: '30px' } }} />
                                <SportsBaseballIcon sx={{ color: '#07484A', fontSize: {xs:'40px',sm:'35px', md: '30px', lg: '30px' } }} />
                                <TwitterIcon sx={{ color: '#07484A', fontSize: {xs:'40px',sm:'35px', md: '30px', lg: '30px' } }} />
                            </Stack>
                            <Typography sx={{ fontWeight: 'bold', color: '#07484A', fontSize: { md: 20, lg: 14 } }}>Address</Typography>
                            <Typography sx={{ width: {xs:'40%',sm:'20%',md:'50%',lg:'25%'}, fontSize: { md: 16, lg: 12 }, mt: 1, color: '#07484A' }}>
                                {each.address}
                            </Typography>
                        </Grid>
                            :
                            <Grid item xs={6} sm={6} md={2} lg={2} sx={{ pl: { sm:0,md: 0, lg: 8},mt:{xs:10,sm:10,md:0,lg:0} }} >
                                <Typography sx={{ fontSize: { md: 20, lg: 14 }, fontWeight: 'bold', color: '#07484A' }}>
                                    {each.name}
                                </Typography>
                                <Typography sx={{ fontSize: {xs:14, md: 16, lg: 14 }, mt: 2, color: '#07484A' }}>
                                    {each.obj1}
                                </Typography>
                                <Typography sx={{ fontSize: {xs:14, md: 16, lg: 14 }, color: '#07484A', mt: 1, }}>
                                    {each.obj2}
                                </Typography>
                                <Typography sx={{ fontSize: {xs:14, md: 16, lg: 14 }, color: '#07484A', mt: 1 }}>
                                    {each.obj3}
                                </Typography>
                            </Grid>


                    )
                })}
                <Typography sx={{ mt: {xs:10,sm:10,md:2,lg:2}, fontSize: {xs:14, md: 16, lg: 12 },color: '#07484A',textAlign:'center' }}>
                    Copyright ©2020 INWOOD. All Rights Reserved
                </Typography>
            </Grid>
        </>
    )
}

export default Footer