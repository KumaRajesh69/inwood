import { Avatar, Box, Button, Card, Grid, Hidden, Link, Rating, Stack, Typography, useMediaQuery } from '@mui/material'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import theme from '../../src/util/theme'
import React from 'react'

function Package() {

    const list = [
        {
            name: 'Larkin Wood Full Set',
            star: '',
            image: '/images/package/pack-img1.svg',
            sub: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            link: 'See Details',
            rs: '$729.99'
        },
        {
            name: 'Larkin Wood Full Set',
            star: '',
            image: '/images/package/pack-img1.svg',
            sub: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            link: 'See Details',
            rs: '$729.99'
        },
        {
            name: 'Larkin Wood Full Set',
            star: '',
            image: '/images/package/pack-img1.svg',
            sub: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            link: 'See Details',
            rs: '$729.99'
        }

    ]


    const [value, setValue] = React.useState(5);

    const mobile = useMediaQuery(theme.breakpoints.only('xs'));
    const tablet = useMediaQuery(theme.breakpoints.only('sm'));
    const desktop = useMediaQuery(theme.breakpoints.up('md'));


    const isMobile = useMediaQuery('(max-width: 600px)');
    const isTablet = useMediaQuery('(min-width: 601px) and (max-width: 1024px)');
    const isLaptop = useMediaQuery('(min-width: 1025px)');


    return (
        <>

            {/* <Grid item direction='column' md='12' sx={{ display: 'flex', justifyContent: 'center' }}> */}
            <Typography sx={{ fontSize: { xs: 25, sm: 50, md: 50, lg: 50 }, fontWeight: 'bold', my: 2, mb: 6, fontFamily: 'Playfair Display', color: '#07484A', textAlign: { xs: 'none', sm: 'center', md: 'center', lg: 'center' } }}>
                Special Package
            </Typography>
            <Hidden xsUp>
                <Stack>
                    <Box sx={{ px: 2, width: '100%', height: '50%' }}>
                        <Stack direction='row' justifyContent='space-between' width='100%'
                            height='100%'
                        >
                            <Typography sx={{ fontSize: 15, fontFamily: 'Playfair Display' }}>
                                Larkin Wood Full Set
                            </Typography>
                            <Typography sx={{ fontWeight: 'bold', fontSize: 15, mr: 6 }}>
                                $729.99
                            </Typography>
                        </Stack>
                        <Rating
                            name="simple-controlled"
                            value={value}
                            onChange={(event, newValue) => {
                                setValue(newValue);
                            }}
                            sx={{ mt: 0.5 }}
                        />

                        <Box>
                            <Link href="#" sx={{ fontSize: 12 }}>See Details</Link>
                        </Box>
                    </Box>
                </Stack>
            </Hidden>
            {/* </Grid> */}
            <Grid container spacing='2' sx={{ my: 6, mt: 8 }}>
                <Grid item md='6' >
                    <Stack sx={{ px: { xs: 0, lg: 10 }, }}>
                        <Card sx={{ borderRadius: 3 }}>

                            {
                                mobile &&
                                <Avatar variant='square'
                                    src='/images/package/mobile.svg'
                                    sx={{ width: '100%', height: '350px' }}
                                />
                            }
                            {
                                tablet &&
                                <Avatar variant='square'
                                    src='/images/package/tablet.svg'
                                    sx={{ width: '100%', height: '350px' }}
                                />
                            }
                            {
                                desktop &&
                                <Avatar variant='square'
                                    src='/images/package/chair5.svg'
                                    sx={{ width: '100%', height: '350px' }}
                                />
                            }

                        </Card>
                        <Stack direction='row' sx={{ justifyContent: 'space-between', mt: 3 }}>
                            <Stack>
                                <Typography sx={{ color: '#07484A', fontSize: 25 }}>
                                    Larkin Wood Full Set
                                </Typography>
                                <Rating
                                    name="simple-controlled"
                                    value={value}
                                    onChange={(event, newValue) => {
                                        setValue(newValue);
                                    }}
                                    sx={{ mt: 2 }}
                                />
                                <Typography sx={{ fontWeight: 'bold', mt: 2, fontSize: 20, color: '#07484A' }}>
                                    $729.99
                                </Typography>
                            </Stack>
                            <Button variant="contained" endIcon={<ShoppingCartIcon style={{ color: '#ffffff' }} />}
                                sx={{
                                    width: '130px',
                                    height: '50px',
                                    bgcolor: '#70908B',
                                    color: '#ffffff',
                                    textTransform: 'capitalize',
                                    fontSize: 11
                                }}>
                                Add to cart
                            </Button>
                        </Stack>
                    </Stack>
                </Grid>
                <Grid item md='6' height='100%'>
                    <Stack sx={{ overflow: 'hidden' }} >
                        <Typography sx={{ fontWeight: 'bold' }}>
                            Description
                        </Typography>
                        <p style={{ width: '80%', fontSize: 12, marginTop: -0.5 }}>
                            Cast Aluminum Outdoor Chaise Lounge As an elegant and classic touch to your
                            outdoor space, this cast Aluminum Chaise Lounge combines the appearance,
                            function and quality all together, offering you with the best experience.
                        </p>
                        <Box
                            sx={{ display: 'flex', flexDirection: 'row', justifycontant: 'center', mt: -1 }}>
                            <Typography sx={{ fontWeight: 'bold', }}>
                                See More
                            </Typography>
                            <KeyboardArrowDownIcon style={{ fontWeight: 'bold' }} />
                        </Box>
                        <Box sx={{ width: '100%', overflow: 'auto', scrollBehavior: 'smooth', ml: 2 }}>
                            <Stack direction={'row'}
                                sx={{ width: { xs: '100%', md: '100%', lg: "100%" } }} spacing={2}>
                                <Stack sx={{ flexDirection: { xs: 'row', sm: 'row', md: 'row', lg: 'column' } }}>
                                    {
                                        list.map((each) => {
                                            return (
                                                <Stack direction='row' sx={{ my: 1, }}>
                                                    <Box >
                                                        <Card>
                                                            <Avatar variant='square' src={each.image} sx={{ height: '110px', width: { xs: '100px', lg: '100%' } }} />
                                                        </Card>
                                                    </Box>
                                                    {/* <Grid item md={8} height={'100%'} > */}
                                                    <Stack direction='row' justifyContent='center' alignItems={'center'} bgcolor='#E0EFF6' borderRadius={2}
                                                        marginX={2}
                                                    >
                                                        <Box sx={{ px: 2, width: '100%' }}>
                                                            <Stack direction='row' justifyContent='space-between' width='100%'
                                                                height='100%'
                                                            >
                                                                <Typography sx={{ fontSize: { xs: 10, lg: 15 }, fontFamily: 'Playfair Display' }}>
                                                                    {each.name}
                                                                </Typography>
                                                                <Typography sx={{ fontWeight: 'bold', fontSize: { xs: 10, lg: 15 }, mr: 6 }}>
                                                                    {each.rs}
                                                                </Typography>
                                                            </Stack>
                                                            <Rating
                                                                name="simple-controlled"
                                                                value={value}
                                                                onChange={(event, newValue) => {
                                                                    setValue(newValue);
                                                                }}
                                                                sx={{ mt: 0.5, fontSize: '14px' }}
                                                            />
                                                            <Typography sx={{ width: '100%', fontSize: 12 }}>
                                                                {each.sub}
                                                            </Typography>
                                                            <Box>
                                                                <Link href="#" sx={{ fontSize: 12, }}>{each.link}</Link>
                                                            </Box>
                                                        </Box>
                                                    </Stack>
                                                    {/* </Grid> */}
                                                </Stack>
                                            )
                                        })
                                    }
                                </Stack>

                            </Stack>
                        </Box>
                    </Stack>


                </Grid>
            </Grid >
        </>

    )
}

export default Package