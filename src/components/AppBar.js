import { Button, Hidden, Stack } from '@mui/material'
import SearchIcon from '@mui/icons-material/Search';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import MenuIcon from '@mui/icons-material/Menu';
import React from 'react'

const list = [
  {
    name: 'Home'
  },
  {
    name: 'Products'
  },
  {
    name: 'Categories'
  },
  {
    name: 'About'
  },
  {
    name: 'Contact Us'
  },
]

function AppBar() {


  return (
    <Stack direction='row'  justifyContent='space-between' alignItems='center' sx={{
      px: { xs: 2, lg: 13 },
       bgcolor: '#F0F0F0',
      width: '100%', 
      height: { xs: '70px' }
    }}>
      <Hidden smUp>
        <MenuIcon sx={{ color: '#07484A' }} />
      </Hidden>
      <img src='./images/appbar/Logo 1.svg' style={{ height: '26px', }} />
      {/* <Hidden smDown>
        <MenuIcon sx={{ color: '#07484A' }} />
      </Hidden> */}
      <Hidden lgDown>
        <Stack direction='row' >
          {list.map((each) => {
            return (
              <Stack direction='row' >
                <Button sx={{
                  fontFamily: 'Playfair Display,serif',
                  textTransform: 'capitalize',
                  fontSize: 14,
                  color: 'black', mx: 1
                }}>
                  {each.name}
                </Button>
              </Stack>
            )
          })}
        </Stack>
      </Hidden>
      <Stack direction='row' spacing={{ xs: 1, md: 5, sm: 5, lg: 5 }} alignItems='center'>
        <SearchIcon sx={{ color: '#07484A' }} />
        <ShoppingCartIcon sx={{ color: '#07484A' }} />
        <AccountCircleIcon sx={{ color: '#07484A' }} />
      </Stack>
    </Stack>
  )
}

export default AppBar