import { Avatar, Box, Button, Grid, Hidden, InputBase, Stack, TextField, Typography, useMediaQuery } from '@mui/material'
import theme from '../../src/util/theme'

import React from 'react'


function Newslatter() {

  const mobile = useMediaQuery(theme.breakpoints.only('xs'));
  const tablet = useMediaQuery(theme.breakpoints.only('sm'));
  const laptop = useMediaQuery(theme.breakpoints.up('md'));
  // const desktop = useMediaQuery(theme.breakpoints.up('lg'));



  return (
    <>
      <Stack sx={{ width: '100%', mt: 10 }}>

        <Grid container sx={{ width: '100%' }}>
          <Grid item xs={12} sm={12} md={6} lg={6}>
            {/* <Avatar variant='square' 
            src='./images/newslatter/newslatter.svg' 
            sx={{ height: '100vh', width: '100%' }} /> */}
            {
              mobile &&
              <Avatar variant='square'
                sx={{ width: '100%', height: '80vh', }}
                src='./images/newslatter/Mobile-newslatter.svg'
              />
            }
            {
              tablet &&
              <Avatar variant='square'
                sx={{ width: '100%', height: '100vh', }}
                src='./images/newslatter/Tablet-newslatter.svg'
              />
            }
            {
              laptop &&
              <Avatar variant='square'
                sx={{ width: '100%', height: '70vh', }}
                src='./images/newslatter/laptop-newslatter.svg'
              />
            }
            {/* {
              desktop &&
              <Avatar variant='square'
                sx={{ width: '100%', height: '100vh',}}
                src='./images/newslatter/newslatter.svg'
              />
            } */}
          </Grid>
          <Grid item xs={12} sm={12} md={6} lg={6}>
            <Stack sx={{
              px: { xs: 2, sm: 5, md: 6, lg: 10 },  
              py: { xs: 5, sm: 10, md: 8, lg: 5 },
              bgcolor: '#E0F6F1',
              justifyContent:{sm:'start',md:'center',lg:'center'},
              alignItems:{sm:'start',md:'center',lg:'center'},
              height: { xs: '100vh', sm: '100vh', md: '70vh', lg: '70hv' }
            }}>
              <Stack sx={{justifyContent:{xs:'center',sm:'start'},alignItems:{xs:'center',sm:'start'}}}>
              <Typography sx={{ fontSize: { xs: 35, sm: 45, md: 50, lg: 50 }, mt: 4, fontFamily: 'Playfair Display', color: '#07484A' }}>
                Join Our
              </Typography>
              <Typography sx={{ fontSize: { xs: 35, sm: 45, md: 50, lg: 50 }, fontWeight: 'bold', fontFamily: 'Playfair Display', color: '#07484A' }}>
                Newsletter
              </Typography>
              <Typography sx={{ fontSize: { xs: 17, sm: 17, md: 17, lg: 17 }, mt: 2, color: '#07484A',textAlign:{xs:'center'} }}>
                Receive exclusive deals, discounts and many offers.
              </Typography>
              <Hidden lgDown>
                <InputBase
                  sx={{ ml: 1,py:{md:4} }}
                  placeholder="Enter your email"
                  InputProps={{
                    style: { fontSize: 30, },
                  }}
                // variant="standard"
                />
              </Hidden>
              <Hidden lgUp>
                <TextField
                  sx={{ ml: 1, flex: 1, my: 2}}
                  placeholder="Enter your email"
                  InputProps={{
                    style: { fontSize: 22 , },
                  }}
                  variant='standard' />
              </Hidden>
              <Button variant="contained"
                sx={{
                  width: { xs: 250, sm: 150, md: 200, lg: 180 },
                  height: {xs:50,sm:50,md:50,lg:50}, bgcolor: '#70908B',
                  color: '#FDFBF8', textTransform: ' capitalize',
                  mt:{md:2,xs:4},fontSize:{xs:20}
                }}>
                Subscribe
              </Button>
              </Stack>
            </Stack>
          </Grid>
        </Grid>
      </Stack>
    </>
  )
}

export default Newslatter