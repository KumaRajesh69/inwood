import { Avatar, Box, Button, Card, Grid, Hidden, Stack, Typography, useMediaQuery } from '@mui/material'
import InputBase from '@mui/material/InputBase';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import MenuIcon from '@mui/icons-material/Menu';
import theme from '../../src/util/theme'
import React from 'react'
import { HideImage, Scale } from '@mui/icons-material';

const cardContents = [
    {
        name: "Bedroom",
        img: '/images/category/bedroom.svg',
        mob: '/images/category/mobile/Bedroom-mobile.svg'
    },
    {
        name: "Dinning",
        img: '/images/category/dinning.svg',
        mob: '/images/category/mobile/Dinning Room-mobile.svg'
    },
    {
        name: "Meeting",
        img: '/images/category/meeting.svg',
        mob: '/images/category/mobile/Kitchen Room-mobile.svg'
    },
    {
        name: "Workspace",
        img: '/images/category/workspace.svg',
        mob: '/images/category/mobile/Living Room-mobile.svg'
    },
    {
        name: "Living Room",
        img: '/images/category/live-room.svg',
        mob: '/images/category/mobile/Meeting Room-mobile.svg'
    },
    {
        name: "Kitchen",
        img: '/images/category/kitchen.svg',
        mob: '/images/category/mobile/Workspace-mobile.svg'
    },

]

const CategoryList = [
    {
        listname: 'Bedroom'
    },
    {
        listname: 'Dinning'
    },
    {
        listname: 'Meeting'
    },
    {
        listname: 'Workspace'
    },
    {
        listname: 'Living Room'
    },
    {
        listname: 'Kitchen'
    },
    {
        listname: 'Living Space'
    },
]

function Category() {
    const mobile = useMediaQuery(theme.breakpoints.only('xs'));
    const tablet = useMediaQuery(theme.breakpoints.only('sm'));
    const desktop = useMediaQuery(theme.breakpoints.up('md'));
    return (
        <Stack alignItems='center' sx={{ my: 8, width: '100%' }}>
            <Typography sx={{ color: '#07484A', fontSize: { xs: 28, sm: 40, md: 40, lg: 50 }, fontWeight: 'bold', fontFamily: 'Playfair Display' }}>
                Explore by Category
            </Typography>
            <Grid container spacing='2' sx={{ mt: 8, px: { xs: 0, sm: 0, md: 5, lg: 10 }, width: '100%' }}>
                <Grid item xs={12} sm={12} md={12} lg={3}
                    width={'100%'} justifyContent='center' alignItems='center'>
                    <Stack sx={{ alignItems: 'start', mt: 2, ml: { xs: 0, lg: 2 }, }}>
                        {/* for xs */}
                        <Hidden lgUp>
                            <Stack direction={'row'} width='100%' justifyContent={{ xs: 'center', lg: 'start' }}
                                alignItems="center" spacing={{ xs: 5, lg: 2 }}>
                                <Stack direction='row'
                                    sx={{
                                        bgcolor: '#F0F0F0', borderRadius: 2, height: { xs: 50, sm: 70 },
                                        width: { xs: '60%', sm: '50%' }
                                    }}>
                                    <IconButton type="button" sx={{ p: '10px' }} >
                                        <SearchIcon />
                                    </IconButton>
                                    <InputBase
                                        sx={{ flex: 1, width: '150px' }}
                                        placeholder="Search"
                                    >
                                    </InputBase>
                                </Stack>
                                <Hidden smUp>
                                    <MenuIcon sx={{ height: 40, width: 40 }} />
                                </Hidden>
                            </Stack>
                        </Hidden>
                        {/* for lg */}
                        <Hidden lgDown>
                            <Box sx={{ bgcolor: '#F0F0F0', borderRadius: 2, alignItems: 'center' }}>
                                <IconButton type="button" sx={{ p: '10px' }} aria-label="search">
                                    <SearchIcon />
                                </IconButton>
                                <InputBase
                                    sx={{ ml: 1, flex: 1 }}
                                    placeholder="Search "
                                >
                                </InputBase>
                            </Box>
                        </Hidden>
                        <Hidden smDown>
                            <Stack sx={{
                                // justifyContent:'start',
                                alignItems: { sm: 'center', lg: 'start' },
                                px: { lg: 0, sm: 20 },
                                width: '100%'
                            }}>
                                <Box width='100%' display={{ lg: 'flex', sm: 'box' }} flexDirection={'column'}
                                    textAlign={{ lg: 'start', sm: 'center' }}

                                >
                                    {CategoryList.map((each) => {
                                        return <Button variant='text' sx={{
                                            fontSize: 12,
                                            fontWeight: 'bold',
                                            color: '#07484A',
                                            textTransform: 'none',
                                            textAlign: { sm: 'center', lg: 'start' }, justifyContent: { lg: 'flex-start', sm: 'center' },
                                            my: 3.5,
                                            width: { sm: '25%', lg: '100%' }
                                        }}>
                                            {each.listname}
                                        </Button>
                                    })}
                                </Box>
                                <Button variant="contained" sx={{ width: '138px', height: '50px', mt: 4, bgcolor: '#70908B' }}>
                                    <Typography sx={{ fontSize: 12, color: '#FDFBF8 ', textTransform: ' capitalize' }}>
                                        All Category
                                    </Typography>
                                </Button>
                            </Stack>
                        </Hidden>
                    </Stack>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={9}  >
                    <Stack direction={'row'}
                        sx={{ width: '100%', }} spacing={2}>
                        <Grid container spacing={2}
                            sx={{ px: 2.5, mt: { xs: 2, sm: 2 } }}
                        >
                            {cardContents.map((card) => {
                                return (
                                    <Grid item xs={12} sm={6} md={6} lg={6}  >
                                        <Stack sx={{
                                            position: 'relative',
                                            zIndex: 0,
                                            '&:hover': {
                                                transform: 'scale(1.05)',
                                            },
                                        }}>
                                            <Card>
                                                {
                                                    mobile &&
                                                    <Avatar variant='square'
                                                        sx={{ width: '100%', height: '25vh', borderRadius: 3 }}
                                                        src={card.mob}
                                                    />
                                                }
                                                {
                                                    tablet &&
                                                    <Avatar variant='square'
                                                        sx={{ width: 355, height: '40vh', borderRadius: 3 }}
                                                        src={card.mob}
                                                    />
                                                }
                                                {
                                                    desktop &&
                                                    <Avatar variant='square'
                                                        sx={{ width: 450, height: 250, borderRadius: 2 }}
                                                        src={card.img}
                                                    />
                                                }
                                                <Stack
                                                    sx={{
                                                        color: '#fff',
                                                        position: 'absolute',
                                                        top: 0,
                                                        left: 0,
                                                        width: '100%',
                                                        height: '100%',
                                                        background: ' linear-gradient(0deg, rgba(40, 51, 113, 0.91), rgba(40, 51, 113, 0.91));',
                                                        display: 'flex',
                                                        flexDirection: 'column',
                                                        justifyContent: 'center',
                                                        textAlign: 'center',
                                                        opacity: 0,
                                                        borderRadius: 3,
                                                        zIndex: 1,
                                                        transition: 'transform .2s',
                                                        p: 2,
                                                        transition: 'transform 0.2s',
                                                        '&:hover': {
                                                            opacity: 0.8,
                                                        },
                                                        overflowY: 'auto',
                                                        overflowX: 'hidden',
                                                        '&::-webkit-scrollbar': {
                                                            width: '0.01em',
                                                        },
                                                        wordBreak: 'break-all'
                                                    }}
                                                >
                                                    <Typography sx={{ fontSize: 40, fontFamily: 'Playfair Display' }}>
                                                        {card?.name}
                                                    </Typography>
                                                    <Box
                                                        sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                                                    >
                                                        <Button variant="contained" sx={{ width: '138px', height: '50px', mt: 2, bgcolor: '#E0EFF6' }}>
                                                            <Typography sx={{ fontSize: 16, color: '#07484A', textTransform: ' capitalize' }}>
                                                                Explore
                                                            </Typography>
                                                        </Button>
                                                    </Box>
                                                </Stack>
                                            </Card>
                                        </Stack>

                                    </Grid>
                                );
                            })}
                        </Grid>
                    </Stack>
                </Grid>
            </Grid>
        </Stack>
    )
}

export default Category


// sx={{
//     height: { xs: 170, sm: 250 },
//     width: { xs: 220, sm: 420 },
//     backgroundImage: `url('${card.img}')`,
//     backgroundSize: '100vh auto',
//     objectFit: 'cover',
//     backgroundPosition: 'center',
//     borderRadius: { xs: 2, sm: 5 },
//     position: 'relative',
//     mt: { xs: 2, sm: 0 },
//     backgroundRepeat: 'no-repeat',

// }}