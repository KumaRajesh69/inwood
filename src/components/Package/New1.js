import { Avatar, Box, Button, Card, Grid, Hidden, Link, Rating, Stack, Typography, useMediaQuery } from '@mui/material'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
// import theme from '../../src/util/theme'
import theme from '../../util/theme';
import React from 'react'

function New1({ size = 12,isLaptop ,isDesktop}) {


    const [value, setValue] = React.useState(5);



    const mobile = useMediaQuery(theme.breakpoints.only('xs'));
    const tablet = useMediaQuery(theme.breakpoints.only('sm'));
    const desktop = useMediaQuery(theme.breakpoints.up('md'));
    console.log('size', size)

    return (
        <>
            <Grid container spacing={2} sx={{ px: { xs: 0, sm: 2, md: 4, lg: 10 }, }}>
                <Grid item lg={size} width={'100%'}>
                    <Stack 
                    // direction={size === 12 ? 'row' : 'column'}
                    direction={{lg:'column',md:'row',sm:'column',xs:'column-reverse'}}
                    >
                        <Box sx={{ width:'100%' ,mt:{xs:4}}}>
                            <Card sx={{ borderRadius: 3 }}>
                                {
                                    mobile &&
                                    <Avatar variant='square'
                                        src='/images/package/mobile.svg'
                                        sx={{ width: '100%', height: '350px' }}
                                    />
                                }
                                {
                                    tablet &&
                                    <Avatar variant='square'
                                        src='/images/package/tablet.svg'
                                        sx={{ width: '100%', height: '350px' }}
                                    />
                                }
                                 {
                                    isLaptop &&
                                    <Avatar variant='square'
                                        src='/images/package/chair5.svg'
                                        sx={{ width: '500px', height: '350px' }}
                                    />
                                }
                                {
                                    isDesktop &&
                                    <Avatar variant='square'
                                        src='/images/package/chair5.svg'
                                        sx={{ width: '100%', height: '350px' }}
                                    />
                                }
                            </Card>
                        </Box>
                        <Box width={'100%'} pl={{xs:0,md:2,lg:0}} >
                            <Stack spacing={2} direction={{xs:'column',lg:'row'}} sx={{ justifyContent: 'space-between', mt: 3 ,width:'100%' }}>
                                <Stack>
                                    <Typography sx={{ color: '#07484A', fontSize: 25 }}>
                                        Larkin Wood Full Set
                                    </Typography>
                                    <Rating
                                        name="simple-controlled"
                                        value={value}
                                        onChange={(event, newValue) => {
                                            setValue(newValue);
                                        }}
                                        sx={{ mt: 2 }}
                                    />
                                    <Typography sx={{ fontWeight: 'bold', mt: 2, fontSize: 20, color: '#07484A' }}>
                                        $729.99
                                    </Typography>
                                </Stack>
                                <Button variant="contained" endIcon={<ShoppingCartIcon style={{ color: '#ffffff' }} />}
                                    sx={{
                                        width: '130px',
                                        height: '50px',
                                        bgcolor: '#70908B',
                                        color: '#ffffff',
                                        textTransform: 'capitalize',
                                        fontSize: 11
                                    }}>
                                    Add to cart
                                </Button>
                            </Stack>
                            <Hidden lgUp xsUp>
                                <Typography sx={{ fontWeight: 'bold' }}>
                                    Description 
                                </Typography>
                                <p style={{ width: '80%', fontSize: 12, marginTop: -0.5 }}>
                                    Cast Aluminum Outdoor Chaise Lounge As an elegant and classic touch to your
                                    outdoor space, this cast Aluminum Chaise Lounge combines the appearance,
                                    function and quality all together, offering you with the best experience.
                                </p>
                            </Hidden>
                            
                        </Box>
                    
                    </Stack>
                </Grid>
            </Grid>
        </>
    )
}

export default New1