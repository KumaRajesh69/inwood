import { Avatar, Box, Link, Rating, Stack, Typography, } from '@mui/material'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
// import theme from '../../src/util/theme'
import React from 'react'

function New2({isDesktop}) {

    const list = [
        {
            name: 'Larkin Wood Full Set',
            star: '',
            image: '/images/package/pack-img1.svg',
            sub: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            link: 'See Details',
            rs: '$729.99'
        },
        {
            name: 'Larkin Wood Full Set',
            star: '',
            image: '/images/package/pack-img1.svg',
            sub: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            link: 'See Details',
            rs: '$729.99'
        },
        {
            name: 'Larkin Wood Full Set',
            star: '',
            image: '/images/package/pack-img1.svg',
            sub: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            link: 'See Details',
            rs: '$729.99'
        },
      


    ]

    const [value, setValue] = React.useState(5);

    return (
        // <Stack direction={'row'}
        //     sx={{ width:'100%' }} spacing={2}>
        // <Stack sx={{ flexDirection: { xs: 'row', sm: 'row', md: 'row', lg: 'column' } }}>
        <Stack direction={{ xs: 'row', sm: 'row', lg: 'column' }} width={isDesktop? '500px':'5px'}>
            {
                list.map((each) => {
                    return (
                        <Stack direction='row' sx={{ my: 1, }}>
                            <Box>
                                <Avatar variant='square' src={each.image}
                                    sx={{ height: '110px', width: { xs: '180px', lg:'10rem' } }}
                                />
                            </Box>
                            <Stack direction='row' justifyContent='center'
                                alignItems={'center'} bgcolor='#E0EFF6' borderRadius={2}
                                marginX={2}
                                width='500px'
                            >
                                <Box sx={{ px: 2, width: '100%' }}>
                                    <Stack direction='row' justifyContent='space-between' width='100%'
                                        height='100%'
                                    >
                                        <Typography 
                                        sx={{ fontSize: { xs: 10, lg: 15 }, fontFamily: 'Playfair Display' }}>
                                            {each.name}
                                        </Typography>
                                        <Typography sx={{ fontWeight: 'bold', fontSize: { xs: 10, lg: 15 }, mr: 6 }}>
                                            {each.rs}
                                        </Typography>
                                    </Stack>
                                    <Rating
                                        name="simple-controlled"
                                        value={value}
                                        onChange={(event, newValue) => {
                                            setValue(newValue);
                                        }}
                                        sx={{ mt: 0.5, fontSize: '14px' }}
                                    />
                                    <Typography sx={{ width: '100%', fontSize: 12 }}>
                                        {each.sub}
                                    </Typography>
                                    <Box>
                                        <Link href="#" sx={{ fontSize: 12, }}>{each.link}</Link>
                                    </Box>
                                </Box>
                            </Stack>
                        </Stack>
                    )
                })
            }
        </Stack>
        // </Stack>
    )
}

export default New2