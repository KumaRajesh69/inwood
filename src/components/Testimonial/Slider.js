import { Avatar, Box, Card, Grid, Stack, Typography } from '@mui/material'
import LinearProgress from '@mui/material/LinearProgress';
import React from 'react'

function Testimonials() {

    
    return (
        <>
            {/* <Stack width='100%' sx={{ justifyContent: 'center', alignItems: 'center', px: { xs: 2, sm: 5, md: 10, lg: 10 } }}>
                <Stack sx={{ justifyContent: 'center', alignItems: 'center', height: '100%', my: 4, color: '#07484A' }}>
                    <Typography sx={{ my: 2, fontFamily: 'Playfair Display', fontSize: 45, fontWeight: 'bold', color: '#07484A' }} variant='h3'>
                        Testimonials
                    </Typography>
                    <Typography sx={{ fontSize: { xs: 20, sm: 25, md: 25, lg: 25 }, color: '#07484A' }}>
                        Over 15,000 happy customers.
                    </Typography>
                </Stack> */}
                {/* <Stack direction='row' sx={{ height: '100%', mt: 8, width: '100%', pl: { md: 0, lg: 15 } }} justifyContent={'center'} alignItems={'center'}> */}
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={7} md={6} lg={4}>
                        <Card sx={{ height: '100%', width: '100%', borderRadius: 2 }}>
                            <Avatar variant='square'
                                src='/images/testimonials/girl.svg'
                                sx={{ height: { xs: '300px', sm: '350px', md: '400px', lg: '300px' }, width: '100%', }} />
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={5} md={6} lg={8}>
                        <Stack sx={{
                            ml: { xs: 0, sm: 2, md: 2, lg: 6 }, my: 2,
                            height: '100%',
                        }} >
                            <Box height='100%'>
                                <Typography sx={{ width: { xs: '100%', sm: '100%', md: '100%', lg: '80%' }, textAlign: 'left', color: '#07484A', fontSize: { xs: 20, sm: 20, md: 18, lg: 25 }, }}>
                                    “My experience with Mark is a complete
                                    sucess, from customer service, wide range of
                                    products, clean store, purchasing experience, the
                                    newsletter.Thank you.”
                                </Typography>
                                <Typography sx={{ mt: 4, fontSize: { xs: 15, sm: 15, md: 15, lg: 15 }, fontWeight: 'bold', color: '#07484A' }}>
                                    Leona Paul
                                </Typography>
                                <Typography sx={{ fontSize: { xs: 15, sm: 15, md: 20, lg: 15 } }}>
                                    CEO of Floatcom
                                </Typography>
                            </Box>
                        </Stack>
                    </Grid>
                </Grid>
                {/* </Stack> */}
                {/* <Box sx={{ width: '100%' }}>
                    <LinearProgress variant="determinate" value={progress} />
                </Box>
            </Stack> */}
        </>
    )
}

export default Testimonials