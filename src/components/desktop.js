import { Avatar, Box, Button, Stack, Typography, useMediaQuery } from '@mui/material'
import theme from '../../src/util/theme'
import React from 'react'

function Desktop() {

  const mobile = useMediaQuery(theme.breakpoints.only('xs'));
  const tablet = useMediaQuery(theme.breakpoints.only('sm'));
  const desktop = useMediaQuery(theme.breakpoints.up('md'));


  return (
    <>
      <Stack width='100%'
        sx={{
          position: 'relative',
          zIndex: 0,
        }}
      >
        {
          mobile &&
          <Avatar variant='square'
            sx={{ width: '100%', height: '100vh',borderRadius:5 }}
            src='./images/desktop/mobile.svg'
          />
        }
        {
          tablet &&
          <Avatar variant='square'
            sx={{ width: '100%', height: '100vh' }}
            src='./images/desktop/desktop-bg.svg'
          />
        }
        {
          desktop &&
          <Avatar variant='square'
            sx={{ width: '100%', height: '100vh',borderRadius:10 }}
            src='./images/desktop/desktop-bg.svg'
          />
        }
        <Stack sx={{
          position: 'absolute',
          zIndex: 1,
          mt: { xs: 10, sm: 20 },
          width: { xs: '100%', sm: '35%', },
          ml: { sm: 13 },
        }}>
          {/* <Stack sx={{justifyContent:'center',textAlign:'center',width:{xs:'100%'}}}> */}
          <Typography sx={{
            width: { xs: '100%', sm: '90%' },
            fontWeight: 'bold', fontSize: { xs: '35px', sm: '45px' }, color: '#07484A', textAlign: { xs: 'center', sm: 'start' },
            fontFamily: 'Playfair Display,serif',
          }}>
            Exclusive Deals of Furniture Collection
          </Typography>
          <Typography sx={{ fontSize: 17, mt: { xs: 2, sm: 4 }, color: '#07484A', textAlign: { xs: 'center', sm: 'start' } }}>
            Explore different categories.Find the best deals.
          </Typography>
          <Stack alignItems={{ xs: 'center', sm: 'start' }}>
            <Button variant="contained" sx={{ width: '150px', height: '50px', mt: 4, bgcolor: '#70908B', borderRadius: 2 }}>
              <Typography sx={{ fontSize: 15, fontWeight: 'bold', color: '#FDFBF8 ', textTransform: ' capitalize', }}>
                Shop Now
              </Typography>
            </Button>
          </Stack>
        </Stack>
      </Stack>
      {/* </Stack> */}
    </>
  )
}
export default Desktop
