import { Avatar, Box, Button, Card, Grid, Hidden, Link, Rating, Stack, Typography, useMediaQuery } from '@mui/material'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import theme from '../../src/util/theme'
import React from 'react'
import New1 from './Package/New1';
import New2 from './Package/New2';

function Package() {

    const list = [
        {
            name: 'Larkin Wood Full Set',
            star: '',
            image: '/images/package/pack-img1.svg',
            sub: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            link: 'See Details',
            rs: '$729.99'
        },
        {
            name: 'Larkin Wood Full Set',
            star: '',
            image: '/images/package/pack-img1.svg',
            sub: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            link: 'See Details',
            rs: '$729.99'
        },
        {
            name: 'Larkin Wood Full Set',
            star: '',
            image: '/images/package/pack-img1.svg',
            sub: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            link: 'See Details',
            rs: '$729.99'
        }

    ]


    const [value, setValue] = React.useState(5);

    // const mobile = useMediaQuery(theme.breakpoints.only('xs'));
    // const tablet = useMediaQuery(theme.breakpoints.only('sm'));
    // const Laptop = useMediaQuery(theme.breakpoints.up('md'));


    const isMobile = useMediaQuery('(max-width: 600px)');
    const isTablet = useMediaQuery('(min-width: 601px) and (max-width: 1023px)');
    const isLaptop = useMediaQuery('(min-width: 1024px) and (max-width: 1100px)');
    const isDesktop = useMediaQuery('(min-width: 1200px)');


    return (
        <>

            <Typography sx={{
                fontSize: { xs: 25, sm: 50, md: 50, lg: 50 },
                fontWeight: 'bold', my: 2, mb: 6, fontFamily: 'Playfair Display',ml:{xs:2},
                color: '#07484A', textAlign: { xs: 'none', sm: 'center', md: 'center', lg: 'center' }
            }}>
                Special Package
            </Typography>
        
            <Stack
            // justifyContent={'center'} alignItems={'center'} width={'100%'}
            >
                <Grid containe >
                    {
                        isDesktop ?
                            <Grid item md={12} >
                                <Stack direction={'row'}>
                                    <New1 isDesktop={isDesktop} />
                                    <Stack>
                                        <Typography sx={{ fontWeight: 'bold' }}>
                                            Description
                                        </Typography>
                                        <p style={{ width: '80%', fontSize: 12, marginTop: -0.5 }}>
                                            Cast Aluminum Outdoor Chaise Lounge As an elegant and classic touch to your
                                            outdoor space, this cast Aluminum Chaise Lounge combines the appearance,
                                            function and quality all together, offering you with the best experience.
                                        </p>
                                        <Box
                                            sx={{ display: 'flex', flexDirection: 'row', justifycontant: 'center', mt: -1 }}>
                                            <Typography sx={{ fontWeight: 'bold', }}>
                                                See More
                                            </Typography>
                                            <KeyboardArrowDownIcon style={{ fontWeight: 'bold' }} />
                                        </Box>
                                        <New2 isDesktop={isDesktop} />
                                    </Stack>
                                </Stack>
                            </Grid>
                            : isLaptop ?
                                <Grid md={12} >
                                    <New1 isLaptop={isLaptop} />
                                    <Hidden lgDown>
                                        <Typography sx={{ fontWeight: 'bold' }}>
                                            Description
                                        </Typography>
                                        <p style={{ width: '80%', fontSize: 12, marginTop: -0.5 }}>
                                            Cast Aluminum Outdoor Chaise Lounge As an elegant and classic touch to your
                                            outdoor space, this cast Aluminum Chaise Lounge combines the appearance,
                                            function and quality all together, offering you with the best experience.
                                        </p>
                                        <Box
                                            sx={{ display: 'flex', flexDirection: 'row', justifycontant: 'center', mt: -1 }}>
                                            <Typography sx={{ fontWeight: 'bold', }}>
                                                See More
                                            </Typography>
                                            <KeyboardArrowDownIcon style={{ fontWeight: 'bold' }} />
                                        </Box>
                                    </Hidden>
                                    <Box sx={{ width: '100%', overflowX: 'auto', scrollBehavior: 'smooth', ml: 2 }}>
                                        <New2 />
                                    </Box>
                                </Grid>
                                : isTablet ?
                                    <Grid md={12} >
                                        <New1 />
                                        <Box sx={{ width: '96%', overflowX: 'auto', scrollBehavior: 'smooth', ml: 2 }}>
                                            <New2 />
                                        </Box>
                                    </Grid>
                                    :
                                    <Grid sm={12} mx={2} >
                                        <New1 size={6} />
                                        <Stack sx={{ overflow: 'hidden' }} >
                                            <Hidden lgDown>
                                                <Typography sx={{ fontWeight: 'bold' }}>
                                                    Description
                                                </Typography>
                                                <p style={{ width: '80%', fontSize: 12, marginTop: -0.5 }}>
                                                    Cast Aluminum Outdoor Chaise Lounge As an elegant and classic touch to your
                                                    outdoor space, this cast Aluminum Chaise Lounge combines the appearance,
                                                    function and quality all together, offering you with the best experience.
                                                </p>
                                                <Box
                                                    sx={{ display: 'flex', flexDirection: 'row', justifycontant: 'center', mt: -1 }}>
                                                    <Typography sx={{ fontWeight: 'bold', }}>
                                                        See More
                                                    </Typography>
                                                    <KeyboardArrowDownIcon style={{ fontWeight: 'bold' }} />
                                                </Box>
                                            </Hidden>
                                            <Hidden xsDown >
                                                <Typography sx={{ fontWeight: 'bold', fontSize: 20, my: 2 }}>
                                                    Description
                                                </Typography>
                                                <p style={{ width: '100%', fontSize: 18, marginTop: -0.5 }}>
                                                    Cast Aluminum Outdoor Chaise Lounge As an elegant and classic touch to your
                                                    outdoor space, this cast...
                                                </p>
                                            </Hidden>
                                            {/* <New2 /> */}
                                        </Stack>
                                    </Grid>
                    }
                </Grid>
            </Stack>

            {/* 
            <Grid container spacing='2' sx={{ my: 6, mt: 8 }}>
                <Grid item sm={12} md={12} lg={6} height='100%'>
                </Grid>
            </Grid > 
            */}
        </>
    )
}
export default Package













{/* <Stack direction='row' sx={{ justifyContent: 'space-between', mt: 3 }}>
{isMobile && (
    <p >Mobile view - Small element</p>
)}
{isTablet && (
    <p >Tablet view - Large element</p>
)}
{isLaptop && 
<></>
}
{isDesktop && (
    <></>

)}

*/}

{/* <Hidden mdUp>
<Stack>
    <Typography sx={{ color: '#07484A', fontSize: 25 }}>
        Larkin Wood Full Set
    </Typography>
    <Rating
        name="simple-controlled"
        value={value}
        onChange={(event, newValue) => {
            setValue(newValue);
        }}
        sx={{ mt: 2 }}
    />
    <Typography sx={{ fontWeight: 'bold', mt: 2, fontSize: 20, color: '#07484A' }}>
        $729.99
    </Typography>
    <Button variant="contained" endIcon={<ShoppingCartIcon style={{ color: '#ffffff' }} />}
        sx={{
            width: '130px',
            height: '50px',
            bgcolor: '#70908B',
            color: '#ffffff',
            textTransform: 'capitalize',
            fontSize: 11
        }}>
        Add to cart
    </Button>
</Stack>
</Hidden> */}