import { Avatar, Box, Grid, Stack, Typography } from '@mui/material'
import React from 'react'

const list = [
    {
        image: './images/benefits/payment.svg',
        name: 'Payment Method',
        about: 'We offer flexible payment options, to make easier.'

    },
    {
        image: './images/benefits/return.svg',
        name: 'Return policy',
        about: 'You can return a product within 30 days.'

    },
    {
        image: './images/benefits/customer.svg',
        name: 'Customer Support',
        about: 'Our customer support',
        abc: ' is 24/7.'

    },
]

function Benefits() {
    return (
        <>
            <Stack sx={{ bgcolor: '#E0EFF6', justifyContent: 'center', alignItems: 'center', width: '100%', my: 4,height:{xs:'100%',sm:'80vh',lg:'80vh'} }}>
              <Grid container spacing={2}>
              <Grid item xs={12} lg={12}>
                <Typography sx={{ 
                    fontSize: {xs:30,sm:45,md:45,lg:45},textAlign:'center',
                     fontWeight: 'bold', color: '#07484A', mt: 2, fontFamily: 'Playfair Display' }}>
                    Benefits for your expediency
                </Typography>
                </Grid>
                <Grid item container  xs={12} sx={{ width: '100%',
                 justifyContent: 'center', alignItems: 'center', my: 6, display:'flex',flexDirection:'row' }}>
                {/* <Stack direction='row' sx={{ width: '100%', justifyContent: 'center', alignItems: 'center', my: 6, px: {xs:10,sm:0,md:5,lg:10} }}> */}
                    {
                        list.map((each) => {
                            return (
                                // <Stack sx={{ justifyContent: 'center', alignItems: 'center', px: 2, width: '100%' }}>
                                <Grid item xs={12} sm={4} lg={4} sx={{ display:'flex',flexDirection:'column',justifyContent: 'center', alignItems: 'center', px: 2, width: '100%' }}>
                                    <Avatar variant='square' src={each.image} sx={{ height: '80px', width: '80px' }} />
                                    <Typography sx={{ fontWeight: 'bold', fontSize: 25, my: 2, color: '#07484A', fontFamily: 'Playfair Display' }}>
                                        {each.name}
                                    </Typography>
                                    <Typography sx={{ px: {xs:8,sm:2,md:5,lg:12}, textAlign: 'center', color: '#07484A',}}>
                                        {each.about}
                                    </Typography>
                                    <Typography sx={{ px: 8, textAlign: 'center', color: '#07484A' }}>
                                        {each.abc}
                                    </Typography>
                               </Grid> 
                                // </Stack>
                            )
                          }
                      )
                    }
                {/* </Stack> */}
                </Grid>
              </Grid>

            </Stack>
        </>
    )
}

export default Benefits