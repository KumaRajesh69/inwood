import { Box, Card, CardActions, Grid, Stack, Typography } from '@mui/material'
import React from 'react'

const cards = [
    {
        img: '/images/creation/img1.svg',
    },
    {
        img: '/images/creation/img2.svg',
    },
    {
        img: '/images/creation/img3.svg',
    },
    {
        img: '/images/creation/img1.svg',
    },
    {
        img: '/images/creation/img2.svg',
    },
    {
        img: '/images/creation/img3.svg',
    },
    {
        img: '/images/creation/img1.svg',
    },
]

function Creation() {
    return (
        <>
            <Stack direction='row' sx={{mt:10,alignItems:'center'}}>
                <Stack sx={{
                    bgcolor: '#70908B',
                    height: '350px',
                    width: '630px',
                    borderRadius: 3, ml: -3,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <Box sx={{ ml: 9 }}>
                        <Typography sx={{ fontSize: 30, fontWeight: 'bold', color: '#FDFBF8', fontFamily: 'Playfair Display', }}>
                            Our<br />
                            Own Creation
                        </Typography >
                        <Typography sx={{ fontSize: 17, color: '#FDFBF8', my: 2 }}>
                            Designed in our studio
                        </Typography>
                        <Typography sx={{ color: '#FDFBF8', fontSize: 10, fontWeight: 'bold' }}>
                            More
                        </Typography>
                    </Box>
                </Stack>
                <Box sx={{ width: '1000px', overflowX: 'auto', scrollBehavior: 'smooth', ml:2}}>
                                <Stack direction={'row'}
                                    sx={{ width: { xs: '700px', md: '700px', lg: "100%" } }} spacing={2}>
                                        {cards.map((card) => {
                                            return (
                                                <Box width='3000px'>
                                                    <Card 
                                                        sx={{
                                                            height: { xs: 250, md: 350 },
                                                            width: "250px",
                                                            backgroundImage: `url('${card.img}')`,
                                                            // backgroundSize: '60vh auto',
                                                            objectFit: 'cover',
                                                            backgroundPosition: 'center',
                                                            borderRadius: 2,
                                                        }}
                                                    >
                                                        <CardActions
                                                            sx={{
                                                                height: '100%',
                                                                flexDirection: 'row',
                                                                width: '100%',
                                                                alignItems: 'end',
                                                            }}
                                                        >
                                                            <Stack>
                                                                <Typography
                                                                    variant='caption'
                                                                    color={'#FFE070'}
                                                                    sx={{ textAlign: 'start', }}>
                                                                    {card?.structure}
                                                                </Typography>
                                                                <Typography variant='subtitle1' color={'#FFFFFF'}>
                                                                    {card?.title}
                                                                </Typography>
                                                            </Stack>
                                                        </CardActions>
                                                    </Card>
                                                </Box>
                                            );
                                        })}
                                </Stack>
                            </Box>
            </Stack>

        </>
    )
}

export default Creation